#!/usr/bin/env python3

import json
import yaml
import argparse
from jinja2 import Template


def render_template(config, template, action):
    l_config = None
    l_template = None
    try:
        l_config = yaml.safe_load(config)
        l_template = yaml.safe_load(template)
    except Exception as exc:
        print(f"ERROR: {exc}")
        exit(1)
    keys = l_template.keys()
    if action not in keys:
        print(f"Action '{action}' is undefined")
        exit(1)
    return Template(l_template[action]).render(**l_config), keys

def main_cli():
    parser = argparse.ArgumentParser()
    parser.add_argument('-c', '--config', type=argparse.FileType('r'), required=True, help="YAML file with config values")
    parser.add_argument('-t', '--template', type=argparse.FileType('r'), required=True, help="YAML file with template")
    parser.add_argument('-a', '--action', required=True, help="Action as it defined in template")
    args = parser.parse_args()

    template, keys = render_template(args.config, args.template, args.action)
    if args.action not in keys:
        parser.error(f"The action '{args.action}' is not defined. Available: {keys}")
    print(template)

if __name__ == "__main__":
    main_cli()

