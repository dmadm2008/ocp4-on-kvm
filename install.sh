#!/bin/bash

INST_CMDS=(
    "prepare-dirs"
    "install-net"
    "install-storage"
    "install-dnsmasq"
    "install-nginx"
    "install-images"
    "install-ipxe"
    "install-ignition"
    "install-vms"
    "install-openshift"
)

DEL_CMDS=(
    "delete-net"
    "delete-storage"
    "delete-dnsmasq"
    "delete-nginx"
    "delete-ipxe"
    "delete-ignition"
    "delete-vms"
    "delete-openshift"
)
#    "delete-instimages"

ACTION=$1

case $ACTION in
    install)
        for cmd in ${INST_CMDS[@]}; do
            sudo_cmd=""
            if [ "$cmd" = "install-nginx" -o "$cmd" = "install-dnsmasq" -o "$cmd" = "prepare-dirs" ]; then
                sudo_cmd="sudo"
            fi
            echo ">>>> [STEP] Executing action: $cmd"
            python3 render.py -t 2template.yaml -c config.yaml -a $cmd | $sudo_cmd bash
        done
        ;;
    delete)
        for cmd in ${DEL_CMDS[@]}; do
            sudo_cmd=""
            if [ "$cmd" = "delete-nginx" -o "$cmd" = "delete-dnsmasq" ]; then
                sudo_cmd="sudo"
            fi
            echo ">>>> [STEP] Executing action: $cmd"
            python3 render.py -t 2template.yaml -c config.yaml -a $cmd | $sudo_cmd bash
        done
        ;;
    *)
        echo "Supported actions: install, delete"
        exit 1
esac


