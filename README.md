# Deployment of Red Hat OpenShift 4.x on LibVirt (bare metal)

OpenShift 4.x installator installs OpenShift 4.x on AWS, Azure, Google Cloud Engine, etc automatically but it also possible to install it on servers physical and virtual. In such case one should prepare the target servers.

If you need to go with virtual machines on KVM hypervisor and use LibVirt, this documents gives an ability to execute this automatically.


## Prerequisites

Assume following.
* There is a server with installed KVM hypervisor and LibVirt on Fedora 31 and you have root access to it. It also should work for Cent OS and Red Hat Linux Enterprise OS.
* A network link is included in a network bridge and it has a connectivity to the Internet.
* We will create VMs for control and data planes from a descriptive config where you can modify some settings.

The automation covers the following operations:

- creating a virtual network
- creating a storage pool
- creating virtual disks
- creating virtual machines
- configuring PXE service
- configuring DNS service
- configuring a load balancer required for OpenShift
- downloading required images for PXE
- executing openshift-install program
- power off and power on virtual machines
- get one or more clusters installed at a time

When you don't need an OpenShift cluster anymore you may undo the made changes:

- deleting virtual machines
- deleting a storage pool
- deleting a virtual network
- removing DNS settings
- removing PXE settings
- removing a load balancer

### Physical Host Network Layout

The pseudo picture illustrates the network topology implemented for this solution. This all is done within a single hypervisor and is not assumed to scale out to more ones.

There can be deployed more than one OpenShift 4.x clusters at a time. They all will have different subnets as well as cluster names.

```
                                                                   \
                                                    +------------+  |
                              10.1.0.1/24           | Master Vms |  |
                               DNS/LB/PXE           +-----o------+  |
                             +-----------+                |         |
                             | br-ocp4-1 o----------------+          > OpenShift 4.x cluster #1
                             +---o-------+                |         |
                                 |                  +-----o------+  |
                192.168.1.210/24 |                  | Worker VMs |  |
                     +--------+  |                  +------------+  |
                     | brext0 o--+                                 /
                     +--------+  |
    Internet <-------o  eno1  |  |
                     +--------+  |
                                 |   10.0.2.0/24
                                 |   +-----+-----+
                                 o---o br-ocp4-2 | OpenShift 4.x cluster #2
                                 |   +-----------+
                                 |   10.0.3.0/24
                                 |   +-----+-----+
                                 o---o br-ocp4-3 | OpenShift 4.x cluster #3
                                 |   +-----------+
                                 |   10.0.X.0/24
                                 |   +-----+-----+
                                 o---o br-ocp4-X | OpenShift 4.x cluster #X
                                     +-----------+
```

## Configuration

The all given configs are implemented with this configuration:

- hypervisor public (physical) ip address: __192.168.1.210/24__
- hypervisor public (physical) bridge name: __brext0___
- base domain for all: __airlan.local__
- OpenShift 4.x cluster network address: __10.1.0.0/24__
- OpenShift 4.x cluster bridge name: __br-ocp4-1__
- OpenShift 4.x cluster gateway IP: __10.1.0.1__



## Prepare a Hypervisor

A hypervisor should should have installed and configured the following software.

### Nginx

Nginx is needed for two reasons:

- to share PXE installation images, scripts, and ignition files
- to behave as a load balancer for OpenShift 4.x api and \*.apps endpoints




File nginx.conf is looking pretty default and adding two __include__ directives:
```
    include             /etc/nginx/conf.d/*.stream;
    include             /etc/nginx/conf.d/*.conf;
```

The entire file looks as this:

```/etc/nginx/nginx.conf
user nginx;
worker_processes auto;
error_log /var/log/nginx/error.log;
pid /run/nginx.pid;
include /usr/share/nginx/modules/*.conf;
events {
    worker_connections 1024;
}
stream {
    include            /etc/nginx/conf.d/*.stream;
}
http {
    log_format  main   '$remote_addr - $remote_user [$time_local] "$request" '
                       '$status $body_bytes_sent "$http_referer" '
                       '"$http_user_agent" "$http_x_forwarded_for"';
    access_log          /var/log/nginx/access.log  main;
    sendfile            on;
    tcp_nopush          on;
    tcp_nodelay         on;
    keepalive_timeout   65;
    types_hash_max_size 4096;
    include             /etc/nginx/mime.types;
    default_type        application/octet-stream;
    include             /etc/nginx/conf.d/*.conf;
}
```


This is a configuration file which created per cluster and is used for installing VMs for OpenShift 4.x:

```/etc/nginx/conf.d/mycluster.myadomain.conf
server {
    listen          10.2.0.1:8080;
    location /images {
        alias       /var/libvirt/images;
        autoindex   on;
    }
    location /ipxe {
        alias       /var/libvirt/ipxe/boot;
        autoindex   on;
    }
    location /ignition {
        alias       /var/libvirt/ignition;
        autoindex   on;
    }
```

This configuration file is created per cluster and it sets up the load balancers
required for an OpenShift 4.x cluster.

```/etc/nginx/conf.d/mycluster.mydomain.conf
upstream intapi22623 {
    hash            consistent;
    server          10.2.0.11:22623;
    server          10.2.0.12:22623;
    server          10.2.0.13:22623;
}
upstream port6443 {
    hash            consistent;
    server          10.2.0.11:6443;
    server          10.2.0.12:6443;
    server          10.2.0.13:6443;
}
upstream port443 {
    hash            consistent;
    server          10.2.0.11:443;
    server          10.2.0.12:443;
    server          10.2.0.13:443;
    server          10.2.0.21:443;
    server          10.2.0.22:443;
    server          10.2.0.23:443;
}

server {
    listen           10.2.0.1:22623;
    proxy_pass       intapi22623;
}
server {
    listen           10.2.0.1:6443;
    proxy_pass       port6443;
}
server {
    listen           192.168.1.210:6443;
    proxy_pass       port6443;
}
server {
    listen           192.168.1.210:443;
    proxy_pass       port443;
}
```


### Dnsmasq

### LibVirt




